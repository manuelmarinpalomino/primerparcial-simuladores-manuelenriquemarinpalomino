﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour
{
    public float TiempoDisparo;
    public Transform puntoDisparo;
    public GameObject bala;
    bool puedoDisparar = true;
    // Update is called once per frame
    void Update()
    {
        if (!SokobanGameManager.gameOver)
        {
            if (puedoDisparar)
            {
                StartCoroutine(Disparo(TiempoDisparo));
            }
        }

    }
    private IEnumerator Disparo (float Tiempo)
    {
        puedoDisparar = false;
        yield return new WaitForSeconds(Tiempo);
        if(!SokobanGameManager.gameOver)
            Instantiate(bala, puntoDisparo.position, Quaternion.identity);
        puedoDisparar = true;
    } 
}
